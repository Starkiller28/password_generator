import string
import random
from django.shortcuts import render
# from django.http import HttpResponse


def home(request):
    # Creamos la vista home de la página web
    return render(request, 'generator/home.html')


def about(request):
    # Creamos la vista about de la página web
    return render(request, 'generator/about.html')


def password(request):
    characters = list(string.ascii_lowercase)
    generated_password = ''
    
    length = int(request.GET.get('length'))# Así obtenemos el valor que se está pasando por la url desde la plantilla home.html al escoger la longitud de la contraseña.
    
    if request.GET.get('uppercase'):
        characters += list(string.ascii_uppercase)
        
    if request.GET.get('numbers'):
        characters += list(string.digits)
        
    if request.GET.get('special'):
        characters += list('!@#$%^&*()_+-')

    for _ in range(length):
        generated_password += random.choice(characters)

    # Así le mandamos la variable generated_password a la plantilla
    return render(request, 'generator/password.html', {'password': generated_password})
